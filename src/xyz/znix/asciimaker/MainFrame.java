/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xyz.znix.asciimaker;

import java.awt.CardLayout;

/**
 *
 * @author znix
 */
public class MainFrame extends javax.swing.JFrame {

	private boolean inExportMode;

	/**
	 * Creates new form MainFrame
	 */
	public MainFrame() {
		initComponents();
	}

	/**
	 * This method is called from within the constructor to initialize the form.
	 * WARNING: Do NOT modify this code. The content of this method is always
	 * regenerated by the Form Editor.
	 */
	@SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jToolBar1 = new javax.swing.JToolBar();
        clearBttn = new javax.swing.JButton();
        rectBttn = new javax.swing.JButton();
        lineBttn = new javax.swing.JButton();
        textBttn = new javax.swing.JButton();
        exportBttn = new javax.swing.JButton();
        contentFlipper = new javax.swing.JPanel();
        textPane = new xyz.znix.asciimaker.AsciiComponent();
        jScrollPane1 = new javax.swing.JScrollPane();
        outputText = new javax.swing.JTextArea();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBounds(new java.awt.Rectangle(50, 50, 250, 250));

        jToolBar1.setRollover(true);

        clearBttn.setText("Clear");
        clearBttn.setFocusable(false);
        clearBttn.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        clearBttn.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        clearBttn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                clearBttnActionPerformed(evt);
            }
        });
        jToolBar1.add(clearBttn);

        rectBttn.setText("Rect");
        rectBttn.setFocusable(false);
        rectBttn.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        rectBttn.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        rectBttn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rectBttnActionPerformed(evt);
            }
        });
        jToolBar1.add(rectBttn);

        lineBttn.setText("Line");
        lineBttn.setFocusable(false);
        lineBttn.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        lineBttn.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        lineBttn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                lineBttnActionPerformed(evt);
            }
        });
        jToolBar1.add(lineBttn);

        textBttn.setText("Text");
        textBttn.setFocusable(false);
        textBttn.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        textBttn.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        textBttn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                textBttnActionPerformed(evt);
            }
        });
        jToolBar1.add(textBttn);

        exportBttn.setText("Export");
        exportBttn.setFocusable(false);
        exportBttn.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        exportBttn.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        exportBttn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                exportBttnActionPerformed(evt);
            }
        });
        jToolBar1.add(exportBttn);

        getContentPane().add(jToolBar1, java.awt.BorderLayout.PAGE_START);

        contentFlipper.setLayout(new java.awt.CardLayout());

        javax.swing.GroupLayout textPaneLayout = new javax.swing.GroupLayout(textPane);
        textPane.setLayout(textPaneLayout);
        textPaneLayout.setHorizontalGroup(
            textPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 444, Short.MAX_VALUE)
        );
        textPaneLayout.setVerticalGroup(
            textPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 378, Short.MAX_VALUE)
        );

        contentFlipper.add(textPane, "edit");

        outputText.setEditable(false);
        outputText.setColumns(20);
        outputText.setRows(5);
        jScrollPane1.setViewportView(outputText);

        contentFlipper.add(jScrollPane1, "export");

        getContentPane().add(contentFlipper, java.awt.BorderLayout.CENTER);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void rectBttnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rectBttnActionPerformed
		textPane.setMode(AsciiComponent.SelectionType.RECTANGLE);
    }//GEN-LAST:event_rectBttnActionPerformed

    private void lineBttnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_lineBttnActionPerformed
		textPane.setMode(AsciiComponent.SelectionType.LINE);
    }//GEN-LAST:event_lineBttnActionPerformed

    private void textBttnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_textBttnActionPerformed
		textPane.setMode(AsciiComponent.SelectionType.POINT);
    }//GEN-LAST:event_textBttnActionPerformed

    private void clearBttnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_clearBttnActionPerformed
		textPane.clear();
    }//GEN-LAST:event_clearBttnActionPerformed

    private void exportBttnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_exportBttnActionPerformed
		inExportMode = !inExportMode;
		CardLayout layout = (CardLayout) contentFlipper.getLayout();
		if (inExportMode) {
			layout.show(contentFlipper, "export");
			outputText.requestFocusInWindow();
			outputText.setFont(textPane.getFont());
			outputText.setText(textPane.getText());
		} else {
			layout.show(contentFlipper, "edit");
			textPane.requestFocusInWindow();
		}
    }//GEN-LAST:event_exportBttnActionPerformed

	/**
	 * @param args the command line arguments
	 */
	public static void main(String args[]) {
		/* Set the Nimbus look and feel */
		//<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
		/* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html
		 */
		try {
			for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
				if ("Nimbus".equals(info.getName())) {
					javax.swing.UIManager.setLookAndFeel(info.getClassName());
					break;
				}
			}
		} catch (ClassNotFoundException ex) {
			java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		} catch (InstantiationException ex) {
			java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		} catch (IllegalAccessException ex) {
			java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		} catch (javax.swing.UnsupportedLookAndFeelException ex) {
			java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		}
		//</editor-fold>

		/* Create and display the form */
		java.awt.EventQueue.invokeLater(new Runnable() {
			public void run() {
				new MainFrame().setVisible(true);
			}
		});
	}

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton clearBttn;
    private javax.swing.JPanel contentFlipper;
    private javax.swing.JButton exportBttn;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JToolBar jToolBar1;
    private javax.swing.JButton lineBttn;
    private javax.swing.JTextArea outputText;
    private javax.swing.JButton rectBttn;
    private javax.swing.JButton textBttn;
    private xyz.znix.asciimaker.AsciiComponent textPane;
    // End of variables declaration//GEN-END:variables
}
