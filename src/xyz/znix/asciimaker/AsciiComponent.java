/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xyz.znix.asciimaker;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.geom.Line2D;
import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.border.BevelBorder;
import sun.font.FontDesignMetrics;

/**
 *
 * @author znix
 */
public class AsciiComponent extends JComponent implements
		MouseListener, MouseMotionListener, KeyListener {

	private final char[][] chars;
	private final Font font;
	private Shape selection;
	private SelectionType mode = SelectionType.RECTANGLE;
	private final Point dragStart;
	private int width;
	private int height;

	public AsciiComponent() {
		SelectionType.parent = this;
		selection = new Rectangle();
		dragStart = new Point();
		font = new Font(Font.MONOSPACED, Font.BOLD, 10);
		width = FontDesignMetrics.getMetrics(font).getWidths()['a'];
		height = font.getSize();
		chars = new char[40][80];
		setFocusable(true);
		addMouseListener(this);
		addMouseMotionListener(this);
		addKeyListener(this);
		setBorder(BorderFactory.createBevelBorder(BevelBorder.LOWERED));
	}

	@Override
	protected void paintComponent(Graphics g) {

		char[] charTmp = {0};

		super.paintComponent(g);
		g.setFont(font);
		int y = 0;
		for (char[] row : chars) {
			int x = 0;
			for (char charr : row) {
				String toDraw;
				if (charr == 0) {
					charTmp[0] = ' ';
				} else {
					charTmp[0] = charr;
				}
				boolean selected = mode.isOnShape(selection, x, y);
				if (selected) {
					g.setColor(Color.blue);
				} else {
					g.setColor(Color.black);
				}
				g.fillRect(x * width, y * height, width, height);
				if (selected) {
					g.setColor(Color.black);
				} else {
					g.setColor(Color.white);
				}
				g.drawChars(charTmp, 0, 1, x * width, y * height + height - 2);
				x++;
			}
			y++;
		}

		g.setColor(Color.black);
		g.drawRect(1, 1, chars[0].length * width, chars.length * height);
	}

	private void getGridPointFromScreen(Point point) {
		point.x /= width;
		point.y /= height;
	}

	private void recomputeSelectionAsBox(Point end) {
		Rectangle r;
		if (selection instanceof Rectangle) {
			r = (Rectangle) selection;
		} else {
			selection = r = new Rectangle();
		}
		checkPoint(dragStart);
		checkPoint(end);
		r.setLocation(dragStart);
		r.setSize(1, 1);
		r.add(end);
		repaint();
	}

	private void recomputeSelectionAsLine(Point end) {
		Line2D.Float l;
		if (selection instanceof Line2D.Float) {
			l = (Line2D.Float) selection;
		} else {
			selection = l = new Line2D.Float();
		}
		checkPoint(dragStart);
		checkPoint(end);
		l.setLine(dragStart, end);
		repaint();
	}

	private void recomputeSelectionAsPoint(Point end) {
		Rectangle r;
		if (selection instanceof Rectangle) {
			r = (Rectangle) selection;
		} else {
			selection = r = new Rectangle();
		}
		checkPoint(end);
		r.setLocation(end);
		r.setSize(1, 1);
		repaint();
	}

	private void checkPoint(Point p) {
		if (p.x < 0) {
			p.x = 0;
		}
		if (p.x >= chars[0].length) {
			p.x = chars[0].length - 1;
		}
		if (p.y < 0) {
			p.y = 0;
		}
		if (p.y >= chars.length) {
			p.y = chars.length - 1;
		}
	}

	public SelectionType getMode() {
		return mode;
	}

	public void setMode(SelectionType mode) {
		if (this.mode == mode) {
			return;
		}
		this.mode = mode;
		mode.recomputeSelection(dragStart);
	}

	@Override
	public Font getFont() {
		return font;
	}

	public String getText() {
		StringBuilder builder = new StringBuilder();
		boolean started = false;
		int endNum = chars.length - 1;
		finder:
		for (int y = chars.length - 1; y >= 0; y--) {
			for (char c : chars[y]) {
				if (c != 0) {
					break finder;
				}
			}
			endNum = y;
		}
		int i = 0;
		for (char[] row : chars) {
			if (i == endNum) {
				break;
			}
			i++;
			StringBuilder b = new StringBuilder();
			int endIndex = 0;
			for (int x = 0; x < row.length; x++) {
				char c = row[x];
				if (c != 0) {
					b.append(row[x]);
					endIndex = x;
				} else {
					b.append(' ');
				}
			}
			b.delete(endIndex + 1, b.length());
			if (!started && b.length() == 0) {
				continue;
			}
			started = true;
			builder.append(b).append('\n');
		}
		return builder.toString();
	}

	public void clear() {
		fill((char) 0);
	}

	public void fill(char c) {
		for (char[] row : chars) {
			for (int x = 0; x < row.length; x++) {
				row[x] = c;
			}
		}
		repaint();
	}

	// awt stuff
	@Override
	public Dimension getMinimumSize() {
		return getPreferredSize();
	}

	@Override
	public Dimension getMaximumSize() {
		return getPreferredSize();
	}

	@Override
	public Dimension getPreferredSize() {
		return new Dimension(chars[0].length * width, chars.length * height);
	}

	// mouse
	@Override
	public void mouseClicked(MouseEvent e) {
	}

	@Override
	public void mousePressed(MouseEvent e) {
		Point point = e.getPoint();
		getGridPointFromScreen(point);
		dragStart.setLocation(point);
		mode.recomputeSelection(point);
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		Point point = e.getPoint();
		getGridPointFromScreen(point);
		mode.recomputeSelection(point);
	}

	@Override
	public void mouseDragged(MouseEvent e) {
		Point point = e.getPoint();
		getGridPointFromScreen(point);
		mode.recomputeSelection(point);
	}

	@Override
	public void mouseMoved(MouseEvent e) {
	}

	@Override
	public void mouseEntered(MouseEvent e) {
	}

	@Override
	public void mouseExited(MouseEvent e) {
	}

	// keys
	@Override
	public void keyTyped(KeyEvent e) {
		char ch = e.getKeyChar();
		if (ch != KeyEvent.CHAR_UNDEFINED) {
			if (ch == ' ') {
				ch = 0;
			}
			for (int y = 0; y < chars.length; y++) {
				for (int x = 0; x < chars[0].length; x++) {
					if (mode.isOnShape(selection, x, y)) {
						chars[y][x] = ch;
					}
				}
			}
			mode.onSelectionUsed();
			repaint();
		}
	}

	@Override
	public void keyPressed(KeyEvent e) {
	}

	@Override
	public void keyReleased(KeyEvent e) {
	}

	public enum SelectionType {
		RECTANGLE {
			@Override
			public void recomputeSelection(Point end) {
				parent.recomputeSelectionAsBox(end);
			}

			@Override
			public boolean isOnShape(Shape s, int x, int y) {
				return ((Rectangle) s).contains(x, y);
			}
		}, LINE {
			@Override
			public void recomputeSelection(Point end) {
				parent.recomputeSelectionAsLine(end);
			}

			@Override
			public boolean isOnShape(Shape s, int x, int y) {
				return ((Line2D.Float) s).ptSegDistSq(x, y) < 0.5;
			}
		}, POINT {
			@Override
			public void recomputeSelection(Point end) {
				parent.recomputeSelectionAsPoint(end);
			}

			@Override
			public void onSelectionUsed() {
				Rectangle r = (Rectangle) parent.selection;
				Point p = r.getLocation();
				p.x++;
				parent.recomputeSelectionAsPoint(p);
			}

			@Override
			public boolean isOnShape(Shape s, int x, int y) {
				return ((Rectangle) s).contains(x, y);
			}
		};

		public static AsciiComponent parent;

		public abstract void recomputeSelection(Point end);

		public void onSelectionUsed() {
		}

		public abstract boolean isOnShape(Shape s, int x, int y);
	}

}
